CREATE TABLE prodotti (
  id int(11) NOT NULL,
  nome varchar(100) NOT NULL,
  prezzo decimal(6,2) DEFAULT '0.00',
  reparto varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO prodotti (id, nome, prezzo, reparto) VALUES
(1, 'The al limone', '1.50', 'Bevande'),
(2, 'Carne', '2.00', 'Macelleria');


CREATE TABLE reparti (
  descrizione varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella reparti
--

INSERT INTO reparti (descrizione) VALUES
('Macelleria'),
('Panetteria'),
('Latticini'),
('Bevande');
