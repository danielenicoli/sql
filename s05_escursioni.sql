drop table if EXISTS foto;
drop table if EXISTS escursioni;
drop table if EXISTS utenti;

create table utenti(
id_utente int not null AUTO_INCREMENT PRIMARY KEY,
nome varchar(50) not null,
    cognome varchar(50) not null,
    email varchar(50) not null,
    password varchar(50) not null
);

create table escursioni(
    id_escursione int PRIMARY KEY not null AUTO_INCREMENT,
    titolo varchar(150) not null,
    descrizione text,
    luogo varchar(150),
    data date,
    commento text,
    id_utente int not null,
    FOREIGN key (id_utente) REFERENCES utenti(id_utente)
    );
    
    create table foto(
    id_foto int PRIMARY KEY not null AUTO_INCREMENT,
    descrizione varchar(100),
    percorso varchar(200) not null,
    id_escursione int not null,
    FOREIGN key (id_escursione) REFERENCES escursioni(id_escursione)
    );