drop table if EXISTS quotazioni;
drop table if EXISTS titoli;
drop TABLE if EXISTS portafogli;
drop table if EXISTS utenti;

create table utenti(
id_utente int not null AUTO_INCREMENT PRIMARY KEY,
nome varchar(50) not null,
    cognome varchar(50) not null,
    email varchar(50) not null,
    password varchar(50) not null
);

create table portafogli(
    id_portafoglio int PRIMARY KEY not null AUTO_INCREMENT,
    descrizione varchar(150) not null,
    id_utente int not null,
    FOREIGN key (id_utente) REFERENCES utenti(id_utente)
    );
    
    create table titoli(
    id_titolo int PRIMARY KEY not null AUTO_INCREMENT,
        sigla varchar(10),
    azienda varchar(150) not null,
    id_portafoglio int not null,
    FOREIGN key (id_portafoglio) REFERENCES portafogli(id_portafoglio)
    );
   
   create table quotazioni(
    id_quotazione int PRIMARY KEY not null AUTO_INCREMENT,
    data_ora datetime not null,
       valore decimal(6,6) not null,
       volume int default 0,
    id_portafoglio int not null,
    FOREIGN key (id_portafoglio) REFERENCES portafogli(id_portafoglio)
    );