drop table if EXISTS utenti;
drop TABLE if EXISTS prodotti;
drop table if EXISTS movimenti;

create table utenti(
id_utente int not null AUTO_INCREMENT PRIMARY KEY,
nome varchar(50) not null,
    cognome varchar(50) not null,
    email varchar(50) not null,
    password varchar(50) not null
);

create table prodotti(
    id_prodotto int PRIMARY KEY not null AUTO_INCREMENT,
    descrizione varchar(150) not null,
    categoria varchar(50),
    peso int not null default 0,
    soglia_kg int DEFAULT 0
    );
    
    create table movimenti(
    id_movimento int not null AUTO_INCREMENT PRIMARY KEY,
        id_prodotto int not null,
        data date not null default current_timestamp,
        quantita int not null default 0,
        carico int not null default 0,
        FOREIGN key (id_prodotto) REFERENCES prodotti(id_prodotto)
    );
    
